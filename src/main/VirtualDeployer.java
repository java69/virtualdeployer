package main;

import java.io.*;
import java.util.Arrays;

import models.ServerBuilder;

public class VirtualDeployer {
	
	public static void main(String[] args) {
		final int REQ_ARGS = 8;
		if(args.length != REQ_ARGS) {
			System.out.println("Usage: VirtualDeployer -s <size> -n <name> -o <os> -i <last ip octate>");
			System.exit(1);
		}
		String size = "", os = "", name = "", ip = "";
		for(int i = 0; i < REQ_ARGS; i++) {
			switch(args[i]){
			case "-s":
				size = args[++i];
				break;
			case "-o":
				os = args[++i];
				break;
			case "-n":
				name = args[++i];
				break;
			case "-i":
				ip = args[++i];
				break;
			}
		}
		System.out.println(size);
		System.out.println(os);
		System.out.println(name);
		System.out.println(ip);
		ServerBuilder builder = new ServerBuilder();
		builder.build(name.trim(), size.trim(), os.trim(), ip.trim());
	}

}
