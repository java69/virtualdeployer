package models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Server {
	private String size = "";
	private String image = "";
	private String name = "";
	private String ip = "";
	private int cpus = 0;
	private int ram = 0;
	private int storage = 0;
	
	public String getSize() {
		return size;
	}

	public Server setSize(String size) {
		this.size = size;
		switch(size) {
			case "small":
				this.cpus = 2;
				this.ram = 4096;
				this.storage = 15;
				break;
			case "medium":
				this.cpus = 4;
				this.ram = 6144;
				this.storage = 30;
				break;
			case "large":
				this.cpus = 8;
				this.ram = 8192;
				this.storage = 60;
				break;
		}
		return this;
	}

	public String getImage() {
		return image;
	}

	public Server setImage(String image) {
		this.image = image;
		return this;

	}
	
	public Server setName(String name) {
		this.name = name;
		return this;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Server setIp(String ip) {
		this.ip = ip;
		return this;
	}

	public void build(){

		System.out.println("Building " + this.size + " " + this.image + " Server");
		String clone = "virt-clone --connect qemu:///system --original "
				+ this.image + " --name " + this.name + " --file /var/lib/libvirt/images/" + this.name + ".qcow2";
		
		String[] setIp = {"sudo", "virt-sysprep", "-a", "/var/lib/libvirt/images/" + this.name + ".qcow2", "--append-line", "/etc/network/interfaces: address 192.168.122." + this.ip};
		String upgradeCpus1 = "virsh -c qemu:///system setvcpus " + this.name + " " + this.cpus + " --config --maximum";
		String upgradeCpus2 = "virsh -c qemu:///system setvcpus " + this.name + " " + this.cpus + " --config";
		
		String upgradeRam1 = "virsh -c qemu:///system setmaxmem " + this.name + " " + this.ram + "M --config";
		String upgradeRam2 = "virsh -c qemu:///system setmem " + this.name + " " + this.ram + "M --config";
		
		// have to ssh in to finalize changes
		String upgradeStorage = "qemu-img resize /var/lib/libvirt/images/" + this.name + ".qcow2 +" + (this.storage - 15);
		
		this.exec(clone);
		
		this.exec(setIp);
		if(!this.size.equals("small")) {
			this.exec(upgradeCpus1);
			this.exec(upgradeCpus2);
			this.exec(upgradeRam1);
			this.exec(upgradeRam2);
			//this.exec(upgradeStorage);
		}
			
			
	}
	
	private void exec(String cmd) {
		try {
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			int stat = p.waitFor();
			System.out.println(cmd + "\nPROCSTAT: " + p.exitValue());
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void exec(String[] cmd) {
		try {
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			int stat = p.waitFor();
			System.out.println("Setting IP..." + "\nPROCSTAT: " + p.exitValue());
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String toString(){
		return "Size: " + this.getSize() +
				"\nImage: " + this.getImage() +
				"\nImagePath: " + this.getImage();
	}

}
