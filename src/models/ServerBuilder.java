package models;

public class ServerBuilder {

	private String[] osOptions = {"debian12", "archlinux", "ubuntu2204"};
	private String[] sizes = {"small", "medium", "large"};
	
	public int build(String name, String size, String os, String ip) {
		
		boolean optFlag = false;
		boolean sizeFlag = false;
		
		name = name.replaceAll("[^A-Za-z0-9]", "");
		
		for(String opt : osOptions) {
			if(optFlag) break;
			if(opt.equals(os)) optFlag = true;
		}
		
		if(!optFlag) {
			System.out.println("Invalid OS. Valid OS options are: ");
			for(String opt : osOptions) {
				System.out.println(opt);
			}
			System.exit(2);
		}
		
		for(String sz : sizes) {
			if(sizeFlag) break;
			if(sz.equals(size)) sizeFlag = true;
		}
		
		if(!sizeFlag) {
			System.out.println("Invalid Size. Valid sizes options are: ");
			for(String sz : sizes) {
				System.out.println(sz);
			}
			System.exit(3);
		}
		
		Server server = new Server();
		
		try {
			server
				.setName(name)
				.setImage(os)
				.setSize(size)
				.setIp(ip)
				.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
}
